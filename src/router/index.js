import {createWebHistory,createRouter} from 'vue-router';
import ChatView from '@/views/ChatView.vue'
import MessageComponent from '@/components/Messages/MessageComponent.vue'
import StatusView from '@/views/StatusView.vue'
import LoginComponent from '@/components/LoginComponent.vue'
import MoreDetails from '@/components/MoreDetails.vue'
import CarouselComponent from '@/components/CarouselComponent.vue'
const routes = [
    {
        path: '/login',
        name: 'Login',
        component: LoginComponent,
        meta:{
            title: 'Login'
        }
    },
    {
        path: '/',
        name:'home',
        component: ChatView ,
        meta: {
            title: 'Chats'
        }
    },
    {
        path: '/chats',
        name:'chats',
        component: ChatView ,
        meta: {
            title: 'Chats'
        }
    },
    {
        path: '/status',
        name:'status',
        component: StatusView ,
        meta: {
            title: 'Status'
        }
    },
    {
        path: '/messages',
        name:'messages',
        component: MessageComponent ,
        meta: {
            title: 'Messages',
            // requireAuth: true
        }
    },
    {
        path: '/more_details',
        name: 'more_details',
        component: MoreDetails,
        meta: {
            title: 'MoreDetails'
        }
    },
    {
        path:'/image_carousel',
        name: 'image_carousel',
        component: CarouselComponent
    }

]
const router = createRouter(
    {
        history: createWebHistory(process.env.BASE_URL),
        mode:'history',
        routes

    }

)
router.beforeEach((to,from,next)=>{
    console.log("running some routing guard checks......")
    document.title = to.meta.title || 'WhatsAppClone'
    if(to.meta.requireAuth){
        const isAuthenticated = localStorage.getItem('my_token')
        if(isAuthenticated){
            next()
        }else{
            next('/login')
        }
    }else{
        next()
    }
})
export default router